package com.sb.parser.model.domain;

import javax.persistence.*;

@Entity(name = "vacancy")
public class Vacancy {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
}
